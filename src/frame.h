#ifndef FRAME_H
#define FRAME_H
#include <stdint.h>
#include <stdlib.h>

typedef uint8_t byte;

enum DCA_TYPE {
    FRAME,
    DIFF,
    ERR,
};

struct dca_pixel {
    byte r, g, b;
};

struct dca_frame {
    enum DCA_TYPE dca_type;
    struct dca_pixel **p;
};

struct dca_frame *dca_frame_new(uint16_t x, uint16_t y)
{
    struct dca_pixel (*p)[x] = calloc(sizeof(int[y][x]));
}

// temp before we write a better one
void dca_frame_free(struct dca_frame *frame)
{
    free(frame);
}

struct dca_diff {
    enum DCA_TYPE dca_type;
    uint16_t x_coord, y_coord;
    struct dca_pixel val;
};

struct diff *calc_diff(struct dca_video *video, struct dca_frame *new_frame)
{

}

union dca_content {
    struct dca_frame *frame;
    struct dca_diff *diff;
};

struct dca_video {
    // Add other fields later, minimal now
    uint16_t x, y;
    dca_fifo df;
};

// Since dca_fifo is not ptr in dca_video,
// dca_fifo_init() is unnecesary.
struct dca_video *dca_video_init(uint16_t x, uint16_t y)
{
    struct dca_video *dv;

    dv = calloc(sizeof(*dv));
    dv->x = x;
    dv->y = y;

    return dv;
}

struct dca_video *dca_video_frame_add(struct dca_video *dv, struct dca_frame *frame)
{
    union dca_content dc;
    dc.frame = frame;

    if (vca_fifo_add(&dv->df, dc) == NULL) {
        return NULL;
    }

    return dv;
}

struct dca_video *dca_video_diff_add(struct dca_video *dv, struct dca_diff *diff)
{
    union dca_content dc;
    dc.diff = diff;

    if (vca_fifo_add(&dv->df, dc) == NULL) {
        return NULL;
    }

    return dv;
}

void dca_video_remove(struct dca_video *dv)
{
    vca_fifo_remove(&dv->df);
}

union vca_content dca_video_peek(struct dca_video *vid)
{
    return vid.df->head->c;
}

/* Linked list to provide queue */
struct dca_ll {
    union dca_content vc;
    struct dca_ll *next;
};

struct dca_ll *dca_ll_add(struct dca_ll *node, union dca_content vc)
{
    struct dca_ll *new = malloc(sizeof(*node));
    if (new == NULL) {
        return NULL;
    }

    new->next = NULL;
    new->vc = vc;

    if (node != NULL) {
        node->next = new;
    }

    return new;
}

void dca_ll_free(struct dca_ll *head)
{
    struct dca_ll *node;

    while (head != NULL) {
        node = head;
        head = head->next;
        free(node);
    }
}

struct dca_fifo {
    uint64_t len;
    struct dca_ll *head;
    struct dca_ll *tail;
};

struct dca_fifo *dca_fifo_init(void)
{
    return calloc(sizeof(struct dca_fifo), 1);
}

void dca_fifo_free(struct dca_fifo *df)
{
    dca_ll_free(df->head);
    free(df);
}

struct dca_fifo *dca_fifo_add(struct dca_fifo *df, union dca_content c)
{
    if (df == NULL) {
        return NULL;
    }

    if (df->head == NULL) {
        df->head = dca_ll_add(NULL, c)
        df->tail = df->head;
    } else {
        df->tail = dca_ll_add(df->tail, c);
    }

    if (df->head == NULL || df->tail == NULL) {
        return NULL;
    }

    df->len++;
    return df;
}

struct dca_fifo *dca_fifo_remove(struct dca_fifo *df)
{
    struct dca_fifo *tmp;
    if (df == NULL || df->head == NULL) {
        return NULL;
    }

    tmp = df->head;
    df->head = df->head->next;
    q->len--;

    free(tmp);

    return q;
}

union vca_content vca_fifo_peek(struct dca_fifo *df)
{
    return df->head->c;
}

#endif /* FRAME_H */